const webpack = require('webpack');
const path = require('path');  
const HtmlwebpackPlugin = require('html-webpack-plugin');

const ROOT_PATH = path.resolve(`${__dirname}/..`);

module.exports = {
  entry: [
    path.resolve(ROOT_PATH, 'src/index.tsx'),
  ],
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.ts$|\.tsx$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'awesome-typescript-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js', '.tsx']
  },
  output: {
    path: path.resolve(ROOT_PATH, 'build'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  watchOptions: {
    ignored: /node_modules/,
    aggregateTimeout: 300,
    poll: 1000
  },
  devServer: {
    contentBase: path.resolve(ROOT_PATH, 'build'),
    historyApiFallback: true,
    progress: true
  },
  plugins: [
    new HtmlwebpackPlugin({
      title: 'Grocerypol',
      template: 'views/index.html'
    })
  ]
}
