import gql from 'graphql-tag'

export const ADD_ITEM = gql`
  mutation addItem($shoppingListId: ID!, $name: String) {
    addItem(shoppingListId: $shoppingListId, name: $name) {
      id
      items {
        name
        checked
        id
      }
    }
  }
`

export const UPDATE_ITEM = gql`
  mutation updateItem($itemId: ID!, $shoppingListId: ID!, $checked: Boolean, $newName: String) {
    updateItem(itemId: $itemId, shoppingListId: $shoppingListId, checked: $checked, newName: $newName) {
      id
      items {
        name
        checked
        id
      }
    }
  }
`

export const REMOVE_ITEM = gql`
  mutation removeItem($itemId: ID!, $shoppingListId: ID!) {
    removeItem(itemId: $itemId, shoppingListId: $shoppingListId) {
      id
      items {
        name
        checked
        id
      }
    }
  }
`
