import gql from 'graphql-tag'

export const UPDATE_SHOPPING_LIST = gql`
  mutation updateShoppingList($shoppingListId: ID!, $name: String) {
    updateShoppingList(shoppingListId: $shoppingListId, name: $name) {
      id
      name
      items {
        name
        checked
        id
      }
    }
  }
`

export const REMOVE_SHOPPING_LIST = gql`
  mutation removeShoppingList($shoppingListId: ID!) {
    removeShoppingList(shoppingListId: $shoppingListId) {
      id
    }
  }
`

export const ADD_SHOPPING_LIST = gql`
  mutation addShoppingList {
    addShoppingList
  }
`
