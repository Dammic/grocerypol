import gql from 'graphql-tag'

export const GET_SHOPPING_LIST_DETAIL = gql`
  query ShoppingList($shoppingListId: ID!) {
    shoppingList(shoppingListId: $shoppingListId) {
      id
      name
      date
      authorId
      items {
        name
        checked
        id
      }
    }
  }
`
