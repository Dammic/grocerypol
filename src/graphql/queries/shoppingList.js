import gql from 'graphql-tag'

export const GET_SHOPPING_LISTS = gql`
  query {
    shoppingLists {
      id,
      name,
      date,
      authorId
    }
  }
`
