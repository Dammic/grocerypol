import * as React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { createGlobalStyle } from 'styled-components'
import { ThemeProvider } from 'styled-components';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo-hooks';

import { Home } from './pages/Home'
import { ShoppingListDetail } from './pages/ShoppingListDetail'
import { NotFound } from './pages/NotFound'
import { pageRoutes } from './pageRoutes'

import 'antd/dist/antd.css'

const GlobalStyle = createGlobalStyle`
  body, html, #root {
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
  }

  #root {
    display: flex;
    justify-content: center;
  }
`

const theme = {
  breakpoints: {
    xs: 0,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
  },
}

const client = new ApolloClient({
  uri: 'http://localhost:3000/graphql',
})

render(
  <ApolloProvider client={client}>
    <GlobalStyle />
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route exact path={pageRoutes.home()} component={Home} />
          <Route exact path={pageRoutes.shoppingListDetail(':shoppingListId')} component={ShoppingListDetail} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById('root')
)
