import * as React from 'react'
import { useQuery } from 'react-apollo-hooks'
import { match } from 'react-router-dom'
import styled from 'styled-components'
import { List } from 'antd'
import { ProductRow } from './components/ProductRow'
import { AddNewItem } from './components/AddNewItem'
import { InfoBox } from './components/InfoBox'
import { GET_SHOPPING_LIST_DETAIL } from '../../graphql'

interface RouterParams {
  shoppingListId: String
}

interface Props {
  match: match<RouterParams>
}

const Container = styled.div`
  padding: 10px;
  max-width: 1080px;
  flex-grow: 1;
`

const ProductsList = styled(List)`
  margin-top: 20px;
`

export const ShoppingListDetail = ({ match: { params: { shoppingListId } } }: Props) => {
  const { data, error, loading } = useQuery(GET_SHOPPING_LIST_DETAIL, { variables: { shoppingListId } })
  const renderItem = React.useCallback((item) => (
    <ProductRow {...item} shoppingListId={shoppingListId} />
  ), [shoppingListId])

  if (loading) {
    return 'loading...'
  }
  const { shoppingList: { name, date, items } } = data

  return (
    <Container>
      <InfoBox shoppingListId={shoppingListId} name={name} createdAtDate={date} />
      <div>Products:</div>
      <ProductsList
        itemLayout="horizontal"
        bordered
        size="small"
        dataSource={items}
        renderItem={renderItem}
        rowKey="id"
        footer={<AddNewItem shoppingListId={shoppingListId} />}
      />
    </Container>
  )
}
