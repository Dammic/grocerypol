import * as React from 'react'
import styled from 'styled-components'
import { Button } from 'antd'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { useMutation } from 'react-apollo-hooks'
import { REMOVE_ITEM } from '../../../../../../graphql'

type Props = {
  shoppingListId: string,
  itemId: string,
}

export const ItemRemoval = ({ shoppingListId, itemId }: Props) => {
  const removeItem = useMutation(REMOVE_ITEM, { variables: { itemId, shoppingListId } })
  return (
    <Button type="dashed" onClick={removeItem as any}>
      <FontAwesomeIcon icon={faTrash} />
    </Button>
  )
}
