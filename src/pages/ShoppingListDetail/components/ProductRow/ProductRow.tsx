import * as React from 'react'
import styled from 'styled-components'
import { Checkbox, List } from 'antd'
import { useMutation } from 'react-apollo-hooks'
import { ProductType } from '../../../../../types'
import { TextInput } from '../../../../components/TextInput'
import { ItemRemoval } from './components/ItemRemoval'
import { UPDATE_ITEM, REMOVE_ITEM } from '../../../../graphql'

interface Props extends ProductType {
  shoppingListId: string
}

const ListItem = styled(List.Item)`
  width: 100%;
  position: relative;
  padding-left: 4px !important;
  padding-right: 4px !important;
`

const StyledCheckbox = styled(Checkbox)`
  flex-shrink: 0;
  &.ant-checkbox-wrapper {
    margin-top: 4px;
  }
`

const InfoBox = styled.div`
  padding: 10px 20px;
  display: flex;
  flex-direction: row;
  width: 100%;
`

const StyledTextInput = styled(TextInput)`
  &.ant-input-disabled {
    background-color: transparent !important;
    border-color: transparent !important;
    text-decoration: line-through;
    &:hover {
      border-color: transparent !important;
    }
  }
`

export const ProductRow: React.FC<Props> = ({ name, checked, id, shoppingListId }) => {
  const [inputValue, setInputValue] = React.useState<string>(name)
  const [checkboxValue, setCheckboxValue] = React.useState<boolean>(checked)

  const variables = { itemId: id, shoppingListId, checked: checkboxValue, newName: inputValue }
  const updateItem = useMutation(UPDATE_ITEM, { variables })
  const removeItem = useMutation(REMOVE_ITEM, { variables: { itemId: id, shoppingListId } })

  const onCheckboxChange = React.useCallback(() => {
    const newValue = !checkboxValue
    setCheckboxValue(newValue)
    updateItem({ variables: { ...variables, checked: newValue } })
  }, [checkboxValue, inputValue])

  const onInputBlur = React.useCallback(() => {
    if (inputValue) {
      updateItem()
    } else {
      removeItem()
    }
  }, [checkboxValue, inputValue])

  return (
    <ListItem>
      <InfoBox>
        <StyledCheckbox checked={checkboxValue} onChange={onCheckboxChange} />
        <StyledTextInput
          value={inputValue}
          autosize
          onChange={e => setInputValue(e.target.value)}
          onBlur={onInputBlur}
          disabled={checkboxValue}
        />
        <ItemRemoval shoppingListId={shoppingListId} itemId={id} />
      </InfoBox>
    </ListItem>
  )
}
