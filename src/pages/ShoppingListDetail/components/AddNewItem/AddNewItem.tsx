import * as React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { useMutation } from 'react-apollo-hooks';
import { TextInput } from '../../../../components/TextInput'
import { ADD_ITEM } from '../../../../graphql'

interface Props {
  shoppingListId: String
}

const AddNewItemElement = styled.div`
  padding: 10px 40px 10px 20px;
  display: flex;
`

const PlusIcon = styled(FontAwesomeIcon)`
  font-size: 19px;
  margin-top: 5px;
`

export const AddNewItem = ({ shoppingListId }: Props) => {
  const [inputValue, setInputValue] = React.useState('')
  const variables = { shoppingListId, name: inputValue }
  const addItem = useMutation(ADD_ITEM, { variables })
  const onInputBlur = React.useCallback(() => {
    if (inputValue === '') {
      return
    }

    addItem()
    setInputValue('')
  }, [inputValue])

  return (
    <AddNewItemElement>
      <PlusIcon icon={faPlus} />
      <TextInput
        value={inputValue}
        placeholder="Add new item"
        autosize
        onChange={e => setInputValue(e.target.value)}
        onBlur={onInputBlur}
      />
    </AddNewItemElement>
  )
}
