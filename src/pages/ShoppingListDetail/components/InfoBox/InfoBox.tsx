import * as React from 'react'
import useReactRouter from 'use-react-router'
import { useMutation } from 'react-apollo-hooks'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { Popconfirm, message, Input, Button } from 'antd'
import styled from 'styled-components'
import { UPDATE_SHOPPING_LIST, REMOVE_SHOPPING_LIST, GET_SHOPPING_LISTS } from '../../../../graphql'
import { DefaultListName } from '../../../../consts'
import { pageRoutes } from '../../../../pageRoutes'

type Props = {
  shoppingListId: string,
  createdAtDate: string,
  name: string,
}

const StyledInfoBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 40px;
`

const StyledInput = styled(Input)`
  max-width: 400px;
`

export const InfoBox = ({ shoppingListId, createdAtDate, name }: Props) => {
  const { history } = useReactRouter()
  const [inputValue, setInputValue] = React.useState<string>(name)
  const [isConfirmVisible, setIsConfirmVisible] = React.useState<boolean>(false)
  const updateVariables = { shoppingListId, name: inputValue }
  const updateShoppingList = useMutation(UPDATE_SHOPPING_LIST, { variables: updateVariables })
  const removeShoppingList = useMutation(REMOVE_SHOPPING_LIST, { variables: { shoppingListId }, refetchQueries: [{ query: GET_SHOPPING_LISTS }] })

  const onConfirm = React.useCallback(async () => {
    await removeShoppingList()
    message.success(`List '${name}' removed successfully!`, 2)
    history.push(pageRoutes.home())
  }, [name])

  const onCancel = React.useCallback(() => {
    setIsConfirmVisible(false)
  }, [])

  const handleVisibleChange = React.useCallback(() => {
    setIsConfirmVisible(!isConfirmVisible)
  }, [isConfirmVisible])

  const onNameChange = React.useCallback(() => {
    updateShoppingList({ variables: { ...updateVariables, name: inputValue || DefaultListName } })
    if (!inputValue) {
      setInputValue(DefaultListName)
    }
  }, [inputValue])

  return (
    <StyledInfoBox>
      <StyledInput value={inputValue} onChange={e => setInputValue(e.target.value)} onBlur={onNameChange} />
      <div>{`Created at: ${createdAtDate}`}</div>
      <Popconfirm
        title="Are you sure you would like to delete the list?"
        visible={isConfirmVisible}
        onConfirm={onConfirm}
        onCancel={onCancel}
        onVisibleChange={handleVisibleChange}
        okText="Yes"
        cancelText="No"
      >
        <Button type="dashed">
          <FontAwesomeIcon icon={faTrash} />
        </Button>
      </Popconfirm>
    </StyledInfoBox>
  )
}
