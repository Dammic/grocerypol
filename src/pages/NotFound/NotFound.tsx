import * as React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;
  font-size: 32px;
  line-height: 40px;
  font-weight: bold;
`

export const NotFound = () => (
  <Container>
    NotFound!
  </Container>
)
