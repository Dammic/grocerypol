import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { pageRoutes } from '../../../../pageRoutes'

export const ListElement = styled.div`
  height: 80px;
  background-color: #f5f0f0;
  box-shadow: 3px 3px 10px 1px rgba(0, 0, 0, 0.25);
  cursor: pointer;
  transition: background-color 0.4s;
  &:hover {
    background-color: #e7dede;
  }
`

const Content = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const ListName = styled.div`
  font-size: 16px;
  line-height: 20px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding-right: 12px;
`

const InfoContainer = styled.div`
  font-size: 14px;
  line-height: 16px;
  display: flex;
  flex-direction: row;
  flex-shrink: 0;
`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;

  &:focus, &:hover, &:visited, &:link, &:active {
      text-decoration: none;
      color: inherit;
  }
`

interface Props {
  name: String,
  date: String,
  authorId: String,
  shoppingListId: String,
}

export const ShoppingListsItem = ({ shoppingListId, name, date, authorId }: Props) => {
  return (
    <StyledLink to={pageRoutes.shoppingListDetail(shoppingListId)}>
      <ListElement>
        <Content>
          <ListName>{name}</ListName>
          <InfoContainer>
            <div>Created at: {date}</div>
          </InfoContainer>
        </Content>
      </ListElement>
    </StyledLink>
  )
}
