import * as React from 'react'
import { useMutation } from 'react-apollo-hooks';
import useReactRouter from 'use-react-router'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { ListElement } from '../ShoppingListsItem'
import { ADD_SHOPPING_LIST } from '../../../../graphql'
import { pageRoutes } from '../../../../pageRoutes'

const AddNewListElement = styled(ListElement)`
  flex-direction: column;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #7d6666;
  font-size: 32px;
`

const AddNewText = styled.div`
  margin-top: 4px;
  font-size: 16px;
  line-height: 20px;
`
export const AddNewList = () => {
  const addShoppingList = useMutation(ADD_SHOPPING_LIST)
  const { history } = useReactRouter()

  const onClick = React.useCallback(async () => {
    const data = await addShoppingList()
    history.push(pageRoutes.shoppingListDetail(data.data.addShoppingList))
  }, [])


  return (
    <AddNewListElement onClick={onClick}>
      <FontAwesomeIcon icon={faPlusCircle} />
      <AddNewText>
        Add new list
      </AddNewText>
    </AddNewListElement>
  )
}
