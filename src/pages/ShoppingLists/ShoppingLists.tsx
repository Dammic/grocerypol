import * as React from 'react'
import { useQuery } from 'react-apollo-hooks';
import styled from 'styled-components'
import breakpoint from 'styled-components-breakpoint';
import { ShoppingListsItem } from './components/ShoppingListsItem'
import { AddNewList } from './components/AddNewList'
import { GET_SHOPPING_LISTS } from '../../graphql'

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: auto;
  gap: 10px;

  ${breakpoint('sm')`
    grid-template-columns: 1fr 1fr;
  `}
`

export const ShoppingLists = () => {
  const { data, error, loading } = useQuery(GET_SHOPPING_LISTS)
  const { shoppingLists } = data

  if (loading) {
    return <span>loading...</span>
  }

  return (
    <GridContainer>
      <AddNewList />
      {shoppingLists.map(({ id, name, date, authorId }) => (
        <ShoppingListsItem
          key={id}
          shoppingListId={id}
          name={name}
          date={date}
          authorId={authorId}
        />
      ))}
    </GridContainer>
  )
}


