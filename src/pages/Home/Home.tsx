import * as React from 'react'
import { ShoppingLists } from '../ShoppingLists'
import styled from 'styled-components'

const Container = styled.div`
  padding: 10px;
  max-width: 1080px;
  flex-grow: 1;
`

export const Home = () => {
  return (
    <Container>
      <ShoppingLists />
    </Container>
  )
}


