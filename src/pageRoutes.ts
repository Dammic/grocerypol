export const pageRoutes = {
  home: (): String => '/',
  shoppingListDetail: (shoppingListId: String): String => `/shopping-list/detail/${shoppingListId}`,
}
