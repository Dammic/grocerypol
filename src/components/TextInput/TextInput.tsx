import React from 'react'
import styled from 'styled-components'
import { Input } from 'antd'

export const TextInput = styled(Input.TextArea)`
  background: rgba(0, 0, 0, 0);
  outline: 0;
  cursor: text;
  resize: none;
  &.ant-input {
    border-color: transparent;
    margin-left: 4px;
    padding-left: 4px;
  }
}`
