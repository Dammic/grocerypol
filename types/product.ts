export interface ProductType {
  name: string,
  checked: boolean,
  id: string,
}

export interface ShoppingListType {
  id: string,
  name: string,
  date: string,
  authorId: string,
  items: ProductType[]
}
